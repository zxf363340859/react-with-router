import React from 'react';

// class ProductDetails extends React.Component{
//
//     constructor(props) {
//         super(props);
//         this.state = {
//             data:
//                 [
//                     {
//                         "id": 1,
//                         "name": "Bicycle",
//                         "price": 30,
//                         "quantity": 15,
//                         "desc": "Bicycle is Good"
//                     },
//                     {
//                         "id": 2,
//                         "name": "TV",
//                         "price": 40,
//                         "quantity": 16,
//                         "desc": "TV is good"
//                     },
//                     {
//                         "id": 3,
//                         "name": "Pencil",
//                         "price": 50,
//                         "quantity": 17,
//                         "desc": "Pencil is good"
//                     }
//                 ]
//         }
//     }
//
//     render() {
//         let result;
//         this.state.data.forEach(value => {
//             if(value.id == this.props.match.params.id)
//                 result = value;
//         });
//         return (
//             <div>
//                 <p>Name: {result.name}</p>
//                 <p>ID: {result.id}</p>
//                 <p>price: {result.price}</p>
//                 <p>quantity: {result.quantity}</p>
//                 <p>desc: {result.desc}</p>
//                 <p>url: /productDetail/{result.id}</p>
//             </div>
//         );
//     }
//
//
// }

const ProductDetails = ({ match }) => {
    return (
        <div>
            {match.params.id}
        </div>
    );
}

export default ProductDetails;
