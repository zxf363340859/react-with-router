import React, {Component} from 'react';
import '../styles/App.css';
import {BrowserRouter as Router, Route, NavLink, Switch} from 'react-router-dom';
import Home from './Home';
import Profile from "./Profile";
import About from "./About";
import '../styles/App.css'
import Products from "./Products";
import ProductDetails from "./ProductDetails";

class App extends Component {
  render() {
    return (
      <div className="app">
        <Router>
          <nav>
            <ul>
              <li>
                <NavLink to="/" activeClassName={'selected'}><span>Home</span></NavLink>
                <NavLink to="/products" activeClassName={'selected'}>Products</NavLink>
                <NavLink to="/my-profile" activeClassName={'selected'}>My Profiles</NavLink>
                <NavLink to="/about-us" activeClassName={'selected'}>About us</NavLink>
              </li>
            </ul>
          </nav>
          <Switch>
            <Route exact path={'/'} component={Home}/>
            <Route path={'/my-profile'} component={Profile}/>
            <Route path={'/(products|goods)'} component={Products}/>
            <Route path={'/about-us'} component={About}/>
            <Route path={'/productDetails/:id'} component={ProductDetails}/>

            <Route component={Home} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
