import React from 'react';
import {Link} from "react-router-dom";

const Products = () => {
    return (
        <div>
            <h1>All products</h1>
            <nav>
                <ul>
                    <li>
                        <Link to={'/productDetails/1'}>Bicycle</Link>
                    </li>
                    <li>
                        <Link to={'/productDetails/2'}>TV</Link>
                    </li>
                    <li>
                        <Link to={'/productDetails/3'}>Pencil</Link>
                    </li>
                </ul>
            </nav>
        </div>
    );
}

export default Products;
